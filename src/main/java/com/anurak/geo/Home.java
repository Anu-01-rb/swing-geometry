/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.anurak.geo;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

/**
 *
 * @author anurak
 */
public class Home extends JPanel{
    
    JLabel hello;
    JLabel intro;
    
    Home(){
        hello = new JLabel("Hello User");
        intro = new JLabel("This is a Geometric Calculation Program");
       
        hello.setHorizontalAlignment(SwingConstants.CENTER);
        intro.setHorizontalAlignment(SwingConstants.CENTER);
        
        hello.setFont(new Font("Serif", Font.PLAIN, 30));
        intro.setFont(new Font("Serif", Font.PLAIN, 18));
        
        this.setLayout(new GridLayout(2,0));
        this.add(hello, BorderLayout.CENTER);
        this.add(intro);
        this.setSize(500, 300);
    }
}
