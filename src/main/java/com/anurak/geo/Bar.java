/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.anurak.geo;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;

/**
 *
 * @author anurak
 */
public class Bar extends JMenuBar{
    JMenu edit;
    JMenuItem file;
    
    Frame frame;
    
    Bar(Frame frame){
        this.frame = frame;
        file = new JMenu("FILE");
        edit = new JMenu("EDIT");
        
        JMenuItem home = new JMenuItem("Home");
        JMenuItem about = new JMenuItem("About");
        
        JMenuItem circle = new JMenuItem("CIRCLE");
        JMenuItem square = new JMenuItem("SQUARE");
        JMenuItem rectangle = new JMenuItem("RECTANGLE");
        JMenuItem triangle =new JMenuItem("TRIANGLE");
        
        file.setFont(new Font("Serif", Font.PLAIN, 12));
        edit.setFont(new Font("Serif", Font.PLAIN, 12));
        home.setFont(new Font("Serif", Font.PLAIN, 12));
        about.setFont(new Font("Serif", Font.PLAIN, 12));
        circle.setFont(new Font("Serif", Font.PLAIN, 12));
        square.setFont(new Font("Serif", Font.PLAIN, 12));
        rectangle.setFont(new Font("Serif", Font.PLAIN, 12));
        triangle.setFont(new Font("Serif", Font.PLAIN, 12));
        
        
        about.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent arg0) {
                frame.setAbout();
            }
        
        });
        
        home.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent arg0) {
                frame.setHome();
            }
        
        });
        
        circle.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent arg0) {
                frame.setCircle();
            }
        
        });
        
        triangle.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent arg0) {
                frame.setTriangle();
            }
        
        });
        
        rectangle.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent arg0) {
                frame.setRectangle();
            }
        
        });
        
        square.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent arg0) {
                frame.setSquare();
            }
        
        });
        
        file.add(home);
        file.add(about);
        
        edit.add(circle);
        edit.add(triangle);
        edit.add(rectangle);
        edit.add(square);
        
        this.add(file);
        this.add(edit);
        
        this.setSize(new Dimension(100,20));
    }
}
