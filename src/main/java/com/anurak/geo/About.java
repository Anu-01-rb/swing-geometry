/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.anurak.geo;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

/**
 *
 * @author anurak
 */
public class About extends JPanel{
    JLabel program;
    JLabel name;
    JLabel school;
    
    
    About(){
        program = new JLabel("SWING : Geometric Calculation Program");
        name = new JLabel("By 63160015 Anurak Yutthanawa");
        school = new JLabel("B.Sc. Computer Science, Burapha University");
        
        program.setFont(new Font("Serif", Font.PLAIN, 12));
        name.setFont(new Font("Serif", Font.PLAIN, 12));
        school.setFont(new Font("Serif", Font.PLAIN, 12));
       
        program.setHorizontalAlignment(SwingConstants.CENTER);
        name.setHorizontalAlignment(SwingConstants.CENTER);
        school.setHorizontalAlignment(SwingConstants.CENTER);
        
        this.setLayout(new GridLayout(3,0));
        this.add(program, BorderLayout.CENTER);
        this.add(name);
        this.add(school);
        this.setSize(500, 300);
    }
}
