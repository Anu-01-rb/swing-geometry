/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.anurak.geo;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import static java.awt.image.ImageObserver.WIDTH;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import shape.Square;

/**
 *
 * @author anurak
 */
public class SquarePanel extends JPanel implements ActionListener{
    
    JButton enter;
    JTextField lengthInput;
    JPanel inputBar;
    String output;
    JLabel areaLabel;
    Frame frame;
//    double length;
    
    SquarePanel(Frame frame){
        JLabel lengthLabel = new JLabel("Input length");
        areaLabel = new JLabel("This square");
        JLabel enterLabel = new JLabel("Click This -->");
        lengthInput = new JTextField();
        inputBar = new JPanel();
        enter = new JButton("Enter");
        this.frame = frame;
        
        areaLabel.setHorizontalAlignment(SwingConstants.CENTER);
        lengthLabel.setHorizontalAlignment(SwingConstants.CENTER);
        enterLabel.setHorizontalAlignment(SwingConstants.CENTER);
        
        lengthLabel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        lengthInput.setBorder(BorderFactory.createLineBorder(Color.BLACK, WIDTH));
        enterLabel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        areaLabel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        
        lengthLabel.setFont(new Font("Serif", Font.PLAIN, 15));
        enterLabel.setFont(new Font("Serif", Font.PLAIN, 15));
        lengthInput.setFont(new Font("Serif", Font.PLAIN, 15));
        enter.setFont(new Font("Serif", Font.PLAIN, 15));
        areaLabel.setFont(new Font("Serif", Font.PLAIN, 15));
        
        enter.addActionListener(this);
        
        inputBar.add(new JLabel());
        inputBar.add(new JLabel());
        inputBar.add(lengthLabel);
        inputBar.add(lengthInput);
        inputBar.add(new JLabel());
        inputBar.add(new JLabel());
        inputBar.add(enterLabel);
        inputBar.add(enter);
        inputBar.add(new JLabel());
        inputBar.add(new JLabel());
        
        inputBar.setLayout(new GridLayout(5,2));
        
        this.add(inputBar);
        this.add(areaLabel);
        this.setLayout(new GridLayout(0,2));
    }

    @Override
    public void actionPerformed(ActionEvent arg0) {
        try{
            double length = Double.parseDouble(lengthInput.getText().toString());
            this.output =  String.format("%s Perimeter: %.2f", (new Square(length)).toString(), length*4);
            areaLabel.setText(this.output);
            frame.pack();
        }catch(Exception e){
            JOptionPane.showMessageDialog(frame, "ERROR! Please Input Number", "ERROR", JOptionPane.ERROR_MESSAGE);
            lengthInput.requestFocus();
        }
    }
    
}
