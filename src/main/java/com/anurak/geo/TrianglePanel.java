/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.anurak.geo;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import shape.Triangle;

/**
 *
 * @author anurak
 */
public class TrianglePanel extends JPanel implements ActionListener{
    
    JButton enter;
    JTextField widthInput;
    JTextField heightInput;
    JPanel inputBar;
    double height;
    double width;
    JLabel areaLabel;
    String output;
    Frame frame;
    
    TrianglePanel(Frame frame){
        JLabel widthLabel = new JLabel("Input width");
        JLabel heightLabel = new JLabel("Input height");
        JLabel enterLabel = new JLabel("Click This -->");
        areaLabel = new JLabel("This triangle");
        widthInput = new JTextField();
        heightInput = new JTextField();
        enter = new JButton("Enter");
        this.frame = frame;
        
        inputBar = new JPanel();
        
        areaLabel.setHorizontalAlignment(SwingConstants.CENTER);
        widthLabel.setHorizontalAlignment(SwingConstants.CENTER);
        heightLabel.setHorizontalAlignment(SwingConstants.CENTER);
        enterLabel.setHorizontalAlignment(SwingConstants.CENTER);
        
        widthLabel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        widthInput.setBorder(BorderFactory.createLineBorder(Color.BLACK, WIDTH));
        heightLabel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        heightInput.setBorder(BorderFactory.createLineBorder(Color.BLACK, WIDTH));
        areaLabel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        
        widthLabel.setFont(new Font("Serif", Font.PLAIN, 15));
        heightLabel.setFont(new Font("Serif", Font.PLAIN, 15));
        enterLabel.setFont(new Font("Serif", Font.PLAIN, 15));
        widthInput.setFont(new Font("Serif", Font.PLAIN, 15));
        heightInput.setFont(new Font("Serif", Font.PLAIN, 15));
        enter.setFont(new Font("Serif", Font.PLAIN, 15));
        areaLabel.setFont(new Font("Serif", Font.PLAIN, 15));
        
        enter.addActionListener(this);
        
        inputBar.add(new JLabel());
        inputBar.add(new JLabel());
        inputBar.add(widthLabel);
        inputBar.add(widthInput);
        inputBar.add(new JLabel());
        inputBar.add(new JLabel());
        inputBar.add(heightLabel);
        inputBar.add(heightInput);
        inputBar.add(new JLabel());
        inputBar.add(new JLabel());
        inputBar.add(enterLabel);
        inputBar.add(enter);
        inputBar.add(new JLabel());
        inputBar.add(new JLabel());
        
        inputBar.setLayout(new GridLayout(7,2));
        
        this.add(inputBar);
        this.add(areaLabel);
        this.setLayout(new GridLayout(0,2));
    }

    @Override
    public void actionPerformed(ActionEvent arg0) {
        try{
            height = Double.parseDouble(heightInput.getText().toString());
            width = Double.parseDouble(widthInput.getText().toString());
            this.output =  String.format("%s Perimeter: %.2f", (new Triangle(width, height)).toString(), width+width+width);
            areaLabel.setText(this.output);
            frame.pack();
        }catch(Exception e){
            JOptionPane.showMessageDialog(frame, "ERROR! Please Input Number", "ERROR", JOptionPane.ERROR_MESSAGE);
            widthInput.requestFocus();
            heightInput.requestFocus();
        }
    }
}
