/*
 * OOP : Object-Oriented Programming 
 * Abstract
 */
package com.anurak.geo;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.SwingConstants;

/**
 *
 * @author Anurak Yutthanawa
 * 63160015 B.Sc.Computer Science
 * Burapha University
 *
 */
public class Frame extends JFrame{
    
    Bar bar;
    Home home;
    About about;
    CirclePanel circle;
    TrianglePanel triangle;
    RectanglePanel rectangle;
    SquarePanel square;
    
    
    Frame(){
        bar = new Bar(this);
        home = new Home();
        about = new About();
        circle = new CirclePanel(this);
        triangle = new TrianglePanel(this);
        rectangle = new RectanglePanel(this);
        square = new SquarePanel(this);
        
        
        bar.setSize(new Dimension(100,10));
        
        setHome();
        
        home.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
//        content.setBackground(Color.gray);
        
//        bar.setLocation(0, 0);
//        content.setLocation(0, 20);
        
        this.setSize(400, 350);
        this.setJMenuBar(bar);
//        this.setLayout(new GridLayout(1,0));
        this.setTitle("Geometry Calculator");
//        this.pack();
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    
    public void setAbout(){
        this.getContentPane().removeAll();
        about.setVisible(true);
        this.setTitle("[About] Geometry Calculator");
        this.add(about);
        this.pack();
        this.setSize(400, 350);
    }
    
    public void setHome(){
        this.getContentPane().removeAll();
        home.setVisible(true);
        this.setTitle("[Home] Geometry Calculator");
        this.add(home);
        this.pack();
        this.setSize(400, 350);
    }
    
    public void setCircle(){
        this.getContentPane().removeAll();
//        circle.setVisible(true);
        this.setTitle("[Circle] Geometry Calculator");
        this.add(circle);
        this.pack();
    }
    
    public void setTriangle(){
        this.getContentPane().removeAll();
//        triangle.setVisible(true);
        this.setTitle("[Triangle] Geometry Calculator");
        this.add(triangle);
        this.pack();
    }
    
    public void setRectangle(){
        this.getContentPane().removeAll();
//        rectangle.setVisible(true);
        this.setTitle("[Rectangle] Geometry Calculator");
        this.add(rectangle);
        this.pack();
    }
    
    public void setSquare(){
        this.getContentPane().removeAll();
//        square.setVisible(true);
        this.setTitle("[Square] Geometry Calculator");
        this.add(square);
        this.pack();
    }
    
}
